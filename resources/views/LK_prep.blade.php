@extends('layouts.auted')
@section('title')
  <title>Личный кабинет</title>
  <meta name="description" content="Сервис онлайн образования">
  <meta name="keyword" content="образование, онлайн курсы, курсы">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width">
@endsection

@section('header')
        <div id="preloder">
          <div class="loader"></div>
        </div>

      <header class="header_field">
        <div class="menu">
          <nav class="main_menu">
            <a href="/" class="btnEx">Выход</a>
            <ul>
              <li ><a href="/subjects">О портале</a></li>
              <li ><a href="/subjects">Предметы</a></li>
              <li><a href="/subjects" >Курсы</a></li>
              <li><a href="#"  class="active">Личный кабинет</a></li>
              <li><a href="#">Контакты</a></li>
            </ul>

          </nav>

        </div>
      </header>
@endsection
@section('content')
  <div class="field">
    <div class="left_side">
      <div class="about_student">

        <img src="img/autors/1.jpg" alt="Pacient A" class="photoStudent">

        <div class="aboutText">
          <h1>Иванова Ирина</h1>
          <h2>Преподаватель</h2>
        </div>
      </div>

      <div class="about_coursePrep">
        <div class="name_course">
          <h1>PRO English  <div class="item_course">Английский язык</div></h1>
          <h2>Курс для углубленного изучения</h2>
        </div>


        <h5>Студенты курса: </h5>
        <div class="prep_course">

          <img src="img/autors/23.jpg" alt="Pacient A" class="photoPrep">

          <div class="prepText">
            <h1>Вольтер Григорий</h1>

          </div>
        </div>

        <div class="prep_course">

          <img src="img/autors/23.jpg" alt="Pacient A" class="photoPrep">

          <div class="prepText">
            <h1>Вольтер Григорий</h1>

          </div>
        </div>
      </div>
    </div>

    <div class="right_side">
      <div class="addText">
        <input type="text" placeholder="Введите сообщение..">
        <a href="#" class="entBtn">Отправить</a>
      </div>

    </div>

  </div>
  <div class="footer"></div>
@endsection
