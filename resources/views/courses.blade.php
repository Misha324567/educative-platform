@extends('layouts.auted')
@section('title')
  <title>Курсы</title>
  <meta name="description" content="Сервис онлайн образования">
  <meta name="keyword" content="образование, онлайн курсы, курсы">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width">
@endsection
@section('header')
        <div id="preloder">
          <div class="loader"></div>
        </div>

      <header class="header_field">
        <div class="menu">
          <nav class="main_menu">
            <a href="/" class="btnEx">Выход</a>
            <ul>
              <li ><a href="/subjects">О портале</a></li>
              <li ><a href="/subjects">Предметы</a></li>
              <li><a href="/courses"  class="active">Курсы</a></li>
              <li><a href="#">Личный кабинет</a></li>
              <li><a href="#">Контакты</a></li>
            </ul>

          </nav>

        </div>
      </header>
@endsection
@section('content')
    <section class="course_field">

        <div class="course-warp">

            <div class="row course_subj_area">
                <div class=" col-lg-3 col-md-4 col-sm-6">
                    <div class="course_subj">
                        <a href="/about"><div class="course-thumb">
                                <img src="img/courses/1.png" class="about_image">
                            </div></a>
                        <div class="course-info">
                            <div class="course-text">
                                <h3>PRO English</h3>
                                <h4>Курс для углубленного изучения </h4>
                                <div class="blockh5"><h5>Повысите уровень английского</h5>
                                    <h5> Улучшите восприятие речи</h5>
                                    <h5> Приобритете полезные для общения навыки</h5>
                                    <h5> Избавитесь от русского акцента</h5></div>

                            </div>
                            <div class="course-author">
                                <figure><img src="img/autors/1.jpg" alt="Pacient A" class="photo"></figure>
                                <p>Иванова И.B.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-lg-3 col-md-4 col-sm-6">
                    <div class="course_subj">
                        <a href="/about"><div class="course-thumb">
                                <img src="img/courses/1.png" class="about_image">
                            </div></a>
                        <div class="course-info">
                            <div class="course-text">
                                <h3>PRO English</h3>
                                <h4>Курс для углубленного изучения </h4>
                                <div class="blockh5"><h5>Повысите уровень английского</h5>
                                    <h5> Улучшите восприятие речи</h5>
                                    <h5> Приобритете полезные для общения навыки</h5>
                                    <h5> Избавитесь от русского акцента</h5></div>

                            </div>
                            <div class="course-author">
                                <figure><img src="img/autors/1.jpg" alt="Pacient A" class="photo"></figure>
                                <p>Иванова И.B.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-lg-3 col-md-4 col-sm-6">
                    <div class="course_subj">
                        <a href="/about"><div class="course-thumb">
                                <img src="img/courses/1.png" class="about_image">
                            </div></a>
                        <div class="course-info">
                            <div class="course-text">
                                <h3>PRO English</h3>
                                <h4>Курс для углубленного изучения </h4>
                                <div class="blockh5"><h5>Повысите уровень английского</h5>
                                    <h5> Улучшите восприятие речи</h5>
                                    <h5> Приобритете полезные для общения навыки</h5>
                                    <h5> Избавитесь от русского акцента</h5></div>

                            </div>
                            <div class="course-author">
                                <figure><img src="img/autors/1.jpg" alt="Pacient A" class="photo"></figure>
                                <p>Иванова И.B.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-lg-3 col-md-4 col-sm-6">
                    <div class="course_subj">
                        <a href="/about"><div class="course-thumb">
                                <img src="img/courses/1.png" class="about_image">
                            </div></a>
                        <div class="course-info">
                            <div class="course-text">
                                <h3>PRO English</h3>
                                <h4>Курс для углубленного изучения </h4>
                                <div class="blockh5"><h5>Повысите уровень английского</h5>
                                    <h5> Улучшите восприятие речи</h5>
                                    <h5> Приобритете полезные для общения навыки</h5>
                                    <h5> Избавитесь от русского акцента</h5></div>

                            </div>
                            <div class="course-author">
                                <figure><img src="img/autors/1.jpg" alt="Pacient A" class="photo"></figure>
                                <p>Иванова И.B.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


