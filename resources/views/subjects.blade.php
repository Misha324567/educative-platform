@extends('layouts.auted')
@section('title')
  <title>Предметы</title>
  <meta name="description" content="Сервис онлайн образования">
  <meta name="keyword" content="образование, онлайн курсы, курсы">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width">
  @endsection

        @section('header')
        <div id="preloder">
          <div class="loader"></div>
        </div>

      <header class="header_field">
        <div class="menu">
          <div class="nav-switch">
            <i class="fa fa-bars"></i>
          </div>
          <nav class="main_menu">
            <a href="/" class="btnEx">Выход</a>
            <ul>
              <li ><a href="/subjects">О портале</a></li>
              <li ><a href="/subjects" class="active">Предметы</a></li>
              <li><a href="/courses">Курсы</a></li>
              <li><a href="#">Личный кабинет</a></li>
              <li><a href="#">Контакты</a></li>
            </ul>

          </nav>

        </div>
      </header>
        @endsection
@section('content')
     <section class="single-course spad pb-0">
    <div class="container">

      <div class="subj_area">

        <div class="row course_subj_area">
          <div class=" col-lg-4 col-md-4 col-sm">

            <div class="course_subj "  >

              <div class="course_name ">
                <div class="course-text">
                  <h5>Английский язык</h5>
                </div>
                <div class="posBtn"><a href="/courses" class="btnCourses">Смотреть курсы</a> </div>
              </div>
            </div>
          </div>
          <div class=" col-lg-4 col-md-4 col-sm-12 ">

            <div class="course_subj "  >

              <div class="course_name">
                <div class="course-text">
                  <h5>История</h5>
                </div>
                <div class="posBtn"><a href="/courses" class="btnCourses">Смотреть курсы</a> </div>
              </div>
            </div>
          </div>
          <div class=" col-lg-4 col-md-4 col-sm-12">
            <div class="course_subj "  >

              <div class="course_name">
                <div class="course-text">
                  <h5>Философия</h5>
                </div>
                <div class="posBtn"><a href="/courses" class="btnCourses">Смотреть курсы</a> </div>
              </div>
            </div>
          </div>
      </div>
    </div>
@endsection
