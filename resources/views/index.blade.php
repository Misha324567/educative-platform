@extends('layouts.app')
@section('style')
    <link href="{{ asset('css/st.css') }}" rel="stylesheet">
@endsection
    @section('title')
  <title>Образовательный сервис</title>
  <meta name="description" content="Сервис онлайн образования">
  <meta name="keyword" content="образование, онлайн курсы, курсы">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width">
    @endsection


@section('content')
  <div id="preloder">
    <div class="loader"></div>
  </div>
  <div class="content_field">
     <div class="content_text">
        <h2 class="text_white">ДОБРО ПОЖАЛОВАТЬ</h2>
        <h2 class="text_pink"> НА СЕРВИС</h2>
        <h2 class="text_pink">ОНЛАЙН ОБРАЗОВАНИЯ!</h2>
      </div>
      <div class="btns">
          <a href="/log" class="btnEntr">Войти</a>
          <a href="/reg" class="btnReg">Зарегистрироваться</a>
      </div>
  </div>

@endsection
