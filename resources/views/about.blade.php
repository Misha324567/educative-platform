 @extends('layouts.auted')
    @section('title')
  <title>PRO English</title>
  <meta name="description" content="Сервис онлайн образования">
  <meta name="keyword" content="образование, онлайн курсы, курсы">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width">
    @endsection
 @section('content')
        <div id="preloder">
          <div class="loader"></div>
        </div>
 @section('header')
      <header class="header_field">
        <div class="menu">
          <nav class="main_menu">
            <a href="/" class="btnEx">Выход</a>
            <ul>
              <li ><a href="/subjects">О портале</a></li>
              <li ><a href="/subjects">Предметы</a></li>
              <li><a href="/courses"  class="active">Курсы</a></li>
              <li><a href="#">Личный кабинет</a></li>
              <li><a href="#">Контакты</a></li>
            </ul>

          </nav>

        </div>
      </header>
 @endsection
 @section('content')
    <section class="single-course spad pb-0">
    <div class="container">

      <div class="course-meta-area">

        <div class="row">

          <div class="col-lg-10 offset-lg-1">
            <div class="nameCourse"><h3>PRO English: курс для углубленного изучения</h3>
              <h5>Предмет: Английский язык</h5></div>
            <img src="img/courses/about1.png" alt="" class="course-preview">

              <div class="info">
                  <h4>О курсе</h4>
                    <p>Онлайн-курс – целенаправленная (обеспечивающая достижение конкретных результатов и направленная на формирование предусмотренных образовательными программами высшего образования компетенций) и определенным образом структурированная совокупность видов, форм и средств учебной деятельности, реализуемая с применением исключительно электронного обучения, дистанционных образовательных технологий на основе комплекса взаимосвязанных в рамках единого педагогического сценария электронных образовательных ресурсов.</p>
              </div>
              <div class="info">
                <h4>О преподавателе</h4>
                <div class="ca-pic" ></div>
                <div class="course-author">
                  <h5>Иванова И.B</h5>
                  <p>Умудренный профессиональным и жизненным опытом педагог понимает, что в знаниях кроется будущая самореализация воспитанников, их успешность.</p>
                </div>
                </div>
              </div>


          </div>
           <div class="priceBtns">
                 <div class="price_btn">Цена</div>
                  <a href="/student" class="buy_btn">Начать обучение</a>
              </div>
        </div>
      </div>

  </section>

    @endsection
