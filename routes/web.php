<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/about', function () {
    return view('about');
});

Route::get('/reg', function () {
    return view('auth.register');
});

Route::get('/log', function () {
    return view('auth.login');
});

Route::get('/courses', function () {
    return view('courses');
});

Route::get('/teacher', function () {
    return view('LK_prep');
});

Route::get('/student', function () {
    return view('LK_student');
});

Route::get('/subjects', function () {
    return view('subjects');
});

Route::get('/', function () {
    return view('index');
});

Auth::routes();


